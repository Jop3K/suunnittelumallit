package proxyteht12;

import java.util.ArrayList;
import java.util.Scanner;

/**
* @author Joni
*/
public class Main {
	public static void main(final String[] arguments) {
		Scanner sc = new Scanner(System.in);
		final Image img1 = new ProxyImage("img1.jpg");
		final Image img2 = new ProxyImage("img2.jpg");
		final Image img3 = new ProxyImage("img3.png");
		final Image img4 = new ProxyImage("img4.jpg");
		
		ArrayList<Image> images = new ArrayList<Image>();
		images.add(img1);
		images.add(img2);
		images.add(img3);
		images.add(img4);
		
		for (Image image : images) {
			image.showData();
		}
		
		
		int currentImage = 0;
		String selection;
		System.out.println(" ");
		images.get(currentImage).displayImage();
		while(true) {
			System.out.println(" ");
			System.out.print("Forward press 'f' or backward press 'b' and to exit press 'q': ");
			selection = sc.next();
			if(selection.equals("f")) {
				if(currentImage == images.size() - 1) {
					currentImage = 0;
				}
				else {
					currentImage++;
				}
			}
			if(selection.equals("b")) {
				if(currentImage == 0) {
					currentImage = images.size() - 1;
				}
				else {
					currentImage--;
				}
			} 
                        if(selection.equals("q")) {
                            break;
                        }
			System.out.println(" ");
			images.get(currentImage).displayImage();
		}
	}
}