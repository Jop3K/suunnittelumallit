package proxyteht12;

//Sys A
class RealImage implements Image {
	private String filename = null;
	
	public RealImage(final String filename) {
		this.filename = filename;
		loadImageFromDisk();
		}

        
        // Lataa tiedoston levyltä
	private void loadImageFromDisk() {
		System.out.println("Loading   " + filename);
	}
	
	// Näyttää kuvan
	public void displayImage() {
		System.out.println("Displaying " + filename);
	}

	public void showData() {
		System.out.println("Filename: " + filename);
	}
}