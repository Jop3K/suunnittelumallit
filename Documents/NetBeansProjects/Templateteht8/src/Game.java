

/**
 *
 * @author Joni
 */
abstract class Game {
    

    abstract void initializeGame();
    abstract void makePlay();


    
    public final void playOneGame(){

        // Luodaan peli ja pelataan yksi peli
        
        initializeGame();

        makePlay();
    }
    
}
