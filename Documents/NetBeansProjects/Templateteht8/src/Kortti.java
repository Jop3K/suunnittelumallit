

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author Joni
 */

public class Kortti extends Game{
    ArrayList RANKS;
    Scanner sc;
    int player1;
    int player2;
    Boolean Peli;
    @Override
    void initializeGame() {
        sc = new Scanner(System.in);
        
        // Luodaan lista korttipakan neljän maan korttien arvoista 1-13
        RANKS = new ArrayList(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9,
            10, 11, 12, 13, 1, 2, 3, 4, 5, 6, 7, 8, 9,
            10, 11, 12, 13, 1, 2, 3, 4, 5, 6, 7, 8, 9,
            10, 11, 12, 13, 1, 2, 3, 4, 5, 6, 7, 8, 9,
            10, 11, 12, 13));
        Peli = true;
    

    
        
    }

    @Override
    void makePlay() {
       
        Collections.shuffle(RANKS);
        System.out.println("Valitkaa monennen kortin haluatte pakasta; korkeampi kortti voittaa.");
        System.out.println("Pelaaja 1 valitse kortti pakasta 1-52");
        System.out.println("Valinta: ");
        int n = sc.nextInt();
        int player1 = (int) RANKS.get(n);
        System.out.println("Pelaaja 2 valitse kortti pakasta 1-52");
        System.out.println("Valinta: ");
        int m = sc.nextInt();
        int player2 = (int) RANKS.get(m);
        
        if(player1 > player2){
            System.out.println("Pelaaja 1 voitti");
            System.out.println("Kortilla: " + player1);

        }else{
            System.out.println("Pelaaja 2 voitti");
            System.out.println("Kortilla: " + player2);
        }
        System.out.println("Pelaajien kortit \n Pelaaja 1: " + player1 +" \n Pelaaja 2: " + player2);
    }

}
