package Memento;

import java.util.Random;

import Memento.Arvuuttaja.Memento;

public class Asiakas implements Runnable {
	Arvuuttaja arvuuttaja;
	Memento memento;
	String name;
	boolean correct;
	int guesses, guess, max, min;
	
	public Asiakas(Arvuuttaja arvuuttaja, String name, int max, int min) {
		this.arvuuttaja = arvuuttaja;
		this.name = name;
		this.max = max;
		this.min = min;
		memento = arvuuttaja.liityPeliin();
		guesses = 0;
	}
	
	private int randomNumber() {
		Random random = new Random();
		int number = random.nextInt(max) + min;
		return number;
	}
	
	private void guess() {
		int guess = randomNumber();
		correct = arvuuttaja.guess(memento, guess, name);
		guesses++;
		if(correct == true) {
			System.out.println(name+" arvasi "+guess+" ja se on oikein!!!");
			System.out.println(name+" arvasi oikein "+guesses+" yrityksellä");
			Thread.currentThread().interrupt();
		}
		else {
			System.out.println(name+" arvasi "+guess+" ja se on väärin");
		}
	}

	@Override
	public void run() {
		while(!Thread.interrupted()) {
			guess();
		}
	}
}
