package Memento;

public class Main {
    
    /*
    * @author Joni
    */
	public static void main(String[] args) {
		int max = 30;
		int min = 1;
		Arvuuttaja arvuuttaja = new Arvuuttaja(max, min);
		Asiakas joni = new Asiakas(arvuuttaja, "Joni", max, min);
		Asiakas tuomas = new Asiakas(arvuuttaja, "Tuomas", max, min);
		Asiakas artur = new Asiakas(arvuuttaja, "Artur", max, min);
		Asiakas joonas = new Asiakas(arvuuttaja, "Joonas", max, min);
		
		new Thread(joni).start();
		new Thread(tuomas).start();
		new Thread(artur).start();
		new Thread(joonas).start();
	}
}