package Memento;

import java.util.Random;

public class Arvuuttaja {
	int max;
        int min;
	
	public Arvuuttaja(int max, int min) {
		this.max = max;
		this.min = min;
	}
	
        // Palautetaan asiakkaalle Memento, jossa on arvottu luku
	public Memento liityPeliin() {
		Memento memento = new Memento(randomNumber());
		return memento;
	}
	
        //random num generator
	private int randomNumber() {
		Random random = new Random();
		int number = random.nextInt(max) + min;
		return number;
	}
	
	public boolean guess(Memento memento, int guess, String name) {
		if(memento.getSavedState() == guess) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public static class Memento {
		private final int number;
		
		public Memento(int numberToSave) {
			number = numberToSave;
		}
		
		private int getSavedState() {
			return number;
		}
	}
}