/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singletonteht5;

/**
 *
 * @author Joni
 */
	import java.util.LinkedList;

	public class GetTheTiles implements Runnable {

	    public void run(){

	           // Luodaan Singleton instanssi

	            Singleton newInstance = Singleton.getInstance();

	            //Uniikki ID instanssille

	            System.out.println("First Instance ID: " + System.identityHashCode(newInstance));

	            // Haetaan kaikki kirjaimet jotka ovat varastoituna listaan

	            System.out.println(newInstance.getLetterList());

	            LinkedList<String> playerOneTiles = newInstance.getTiles(7);

	            System.out.println("Pelaaja 1: " + playerOneTiles);

	        System.out.println("Got Tiles");

	    }

	     

	}
