/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singletonteht5;

/**
 *
 * @author Joni
 */
import java.util.LinkedList;

	public class ScrabbleTest {

	    public static void main(String[] args){

	        

                // Luodaan kaksi instanssia, annetaan pelaajille palikat ja tulostetaan listat
                
  
                
	        Singleton newInstance = Singleton.getInstance();

	        System.out.println("First instance ID: " + System.identityHashCode(newInstance));


	        System.out.println(newInstance.getLetterList());
                
                // Pelaaja 1 saa 7 palikkaa

	        LinkedList<String> playerOneTiles = newInstance.getTiles(7);

	        System.out.println("Pelaaja 1: " + playerOneTiles);

	        System.out.println(newInstance.getLetterList());
	    

	        Singleton instanceTwo = Singleton.getInstance();


	        System.out.println("Second instance ID: " + System.identityHashCode(instanceTwo));

                
	        System.out.println(instanceTwo.getLetterList());

	        // Pelaaja 2 saa 7 palikkaa

	        LinkedList<String> playerTwoTiles = newInstance.getTiles(7);

	        System.out.println("Pelaaja 2: " + playerTwoTiles);

	    }

	}
