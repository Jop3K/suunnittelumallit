/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singletonteht5;

/**
 *
 * @author Joni
 */
import java.util.Arrays;

	import java.util.Collections;

	import java.util.LinkedList;

	public class Singleton {

	    private static Singleton firstInstance = null;
	     
            // asetetaan scrabblen kirjaimet listaan
            
	    String[] scrabbleLetters = {"a", "a", "a", "a", "a", "a", "a", "a", "a",

	            "b", "b", "c", "c", "d", "d", "d", "d", "e", "e", "e", "e", "e",

	            "e", "e", "e", "e", "e", "e", "e", "f", "f", "g", "g", "g", "h",

	            "h", "i", "i", "i", "i", "i", "i", "i", "i", "i", "j", "k", "l",

	            "l", "l", "l", "m", "m", "n", "n", "n", "n", "n", "n", "o", "o",

	            "o", "o", "o", "o", "o", "o", "p", "p", "q", "r", "r", "r", "r",

	            "r", "r", "s", "s", "s", "s", "t", "t", "t", "t", "t", "t", "u",

	            "u", "u", "u", "v", "v", "w", "w", "x", "y", "y", "z",}; 

	    private LinkedList<String> letterList = new LinkedList<String> (Arrays.asList(scrabbleLetters));


	    static boolean firstThread = true;


	    private Singleton() { }
	     

	    public static Singleton getInstance() {

	        if(firstInstance == null) {

	           

	            if(firstThread){

	                firstThread = false;

	                try {

	                    Thread.currentThread();

	                    Thread.sleep(1000);

	                } catch (InterruptedException e) {

	                    e.printStackTrace();

	                }

	            }

	          

	            synchronized(Singleton.class){

                        // Jos instanssia ei tarvita, sitä ei luoda
	                if(firstInstance == null) {


	                    firstInstance = new Singleton();

	                    // Sekoitetaan kirjaimet listassa

	                    Collections.shuffle(firstInstance.letterList);

	                }

	            }

	        }


	        // Palautetaan instanssi kummassakin tapauksessa
	        return firstInstance;

	    }

            // Luodaan getteri kirjain listalle
	    public LinkedList<String> getLetterList(){

	        return firstInstance.letterList;

	    }

            
	    public LinkedList<String> getTiles(int howManyTiles){

	 
	        LinkedList<String> tilesToSend = new LinkedList<String>();

	   

	        for(int i = 0; i <= howManyTiles; i++){

	            tilesToSend.add(firstInstance.letterList.remove(0));

	        }

	

	        return tilesToSend;

	    }

	}