

public class MyPokemon {

    private MyPokemonState state;

    private int level = 1;

    private int hp = 10;
    private int speed = 5;
    private int strengh = 4;
    private int attack = 3;
    private int defence = 4;

    public MyPokemon() {
        state = Charmander.INSTANCE;
    }

    protected void evolve(MyPokemonState state) {
        this.state = state;
    }

    void levelUp() {
        state.levelUp(this);
    }

    void run() {
        state.run(this);
    }

    void walk() {
        state.walk(this);
    }

    void fly() {
        state.fly(this);
    }

    void attack() {
        state.attack(this);
    }

    void defend() {
        state.defend(this);
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getHp() {
        return hp;
    }

    public int getSpeed() {
        return speed;
    }

    public int getStrengh() {
        return strengh;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefence() {
        return defence;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setStrengh(int strengh) {
        this.strengh = strengh;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

}
