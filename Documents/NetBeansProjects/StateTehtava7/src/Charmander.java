

public enum Charmander implements MyPokemonState {

    INSTANCE;

    private Charmander() {
    }

    @Override
    public void levelUp(MyPokemon myPokemon) {
        if (myPokemon.getLevel() < 16) {
            myPokemon.setLevel(myPokemon.getLevel() + 1);
            if (myPokemon.getLevel() == 16) {
                evolve(myPokemon, Charmeleon.INSTANCE);
                System.out.println("Charmanders' level is " + myPokemon.getLevel() + " and it evolves into Charmeleon!");
            } else {
                System.out.println("Charmanders' level is " + myPokemon.getLevel());
            }
        }
    }

    @Override
    public void run(MyPokemon myPokemon) {
        System.out.println("Charmander runs");
    }

    @Override
    public void walk(MyPokemon myPokemon) {
        System.out.println("Charmander walks");
    }

    @Override
    public void fly(MyPokemon myPokemon) {
        System.out.println("I don't have wings!");
        walk(myPokemon);
    }

    @Override
    public void attack(MyPokemon myPokemon) {
        blaze();
    }

    @Override
    public void defend(MyPokemon myPokemon) {
        System.out.println("Charmander defends");
    }

    public void blaze() {
        System.out.println("Blaze attack");
    }

}
