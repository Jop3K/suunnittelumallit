public class Main {

    /*
    * @author Joni
    */
    
    public static void main(String[] args) {
        
        MyPokemon pokemon = new MyPokemon();
        
        pokemon.walk();
        pokemon.run();
        pokemon.fly();
        pokemon.attack();
        pokemon.defend();
        pokemon.attack();
        
        pokemon.setLevel(14);
        pokemon.levelUp();
        pokemon.levelUp();
        pokemon.levelUp();
        pokemon.levelUp();
        pokemon.levelUp();
        pokemon.levelUp();
        
        
        pokemon.walk();
        pokemon.run();
        pokemon.fly();
        pokemon.attack();
        pokemon.defend(); 
        pokemon.attack();
        
        pokemon.setLevel(33);
        pokemon.levelUp();
        pokemon.levelUp();
        pokemon.levelUp();
        pokemon.levelUp();
        pokemon.levelUp();
        pokemon.levelUp();
        
        pokemon.walk();
        pokemon.run();
        pokemon.fly();
        pokemon.attack();
        pokemon.defend();
        pokemon.attack();
        
        pokemon.setLevel(98);
        
        pokemon.levelUp();
        pokemon.levelUp();
        pokemon.levelUp();
  

    }

}
