

public interface MyPokemonState {

    void levelUp(MyPokemon myPokemon);

    void run(MyPokemon myPokemon);

    void walk(MyPokemon myPokemon);

    void fly(MyPokemon myPokemon);

    void attack(MyPokemon myPokemon);

    void defend(MyPokemon myPokemon);

    default void evolve(MyPokemon pokemon, MyPokemonState state) {
        pokemon.evolve(state);
        pokemon.setAttack(pokemon.getAttack() + 4);
        pokemon.setDefence(pokemon.getDefence() + 5);
        pokemon.setHp(pokemon.getHp() + 8);
        pokemon.setSpeed(pokemon.getSpeed() + 3);
        pokemon.setStrengh(pokemon.getStrengh() + 4);
    }

}
