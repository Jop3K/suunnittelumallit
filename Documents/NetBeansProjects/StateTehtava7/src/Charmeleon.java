

public enum Charmeleon implements MyPokemonState {

    INSTANCE;

    private Charmeleon() {
    }

    @Override
    public void levelUp(MyPokemon myPokemon) {
        if (myPokemon.getLevel() < 36) {
            myPokemon.setLevel(myPokemon.getLevel() + 1);
            if (myPokemon.getLevel() == 36) {
                evolve(myPokemon, Charizard.INSTANCE);
                System.out.println("Charmeleons' level is " + myPokemon.getLevel() + " and it evolves into Charizard!");
            } else {
                System.out.println("Charmeleons' level is " + myPokemon.getLevel());
            }
        }
    }

    @Override
    public void run(MyPokemon myPokemon) {
        System.out.println("Pokemon runs fast");
    }

    @Override
    public void walk(MyPokemon myPokemon) {
        System.out.println("Charmeleon walks");
    }

    @Override
    public void fly(MyPokemon myPokemon) {
        System.out.println("I don't have wings!");
        run(myPokemon);
    }

    @Override
    public void attack(MyPokemon myPokemon) {
        blaze();
    }

    @Override
    public void defend(MyPokemon myPokemon) {
        callForSupport();
    }
    
    public void blaze() {
        System.out.println("Blaze attack");
    }
    
    public void callForSupport() {
        System.out.println("Charmeleon calls for support");
    }

}
