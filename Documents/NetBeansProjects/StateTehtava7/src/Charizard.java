

public enum Charizard implements MyPokemonState {

    INSTANCE;

    private Charizard() {
    }

    @Override
    public void levelUp(MyPokemon myPokemon) {
        if (myPokemon.getLevel() < 100) {
            myPokemon.setLevel(myPokemon.getLevel() + 1);
            System.out.println("Charizards' level is " + myPokemon.getLevel());
        } else {
            System.out.println("Max Level");
        }
    }

    @Override
    public void run(MyPokemon myPokemon) {
        System.out.println("Charizard runs");
    }

    @Override
    public void walk(MyPokemon myPokemon) {
        System.out.println("Charizard walks");
    }

    @Override
    public void fly(MyPokemon myPokemon) {
        System.out.println("Charizard flies");
    }

    @Override
    public void attack(MyPokemon myPokemon) {
        fireBlast();
        thoughClaws();
    }

    @Override
    public void defend(MyPokemon myPokemon) {
        System.out.println("Charizard defends");
    }
    
    public void fireBlast() {
        System.out.println("Fire blast attack");
    }
    public void thoughClaws() {
        System.out.println("Though Claws attack");
    }

}
