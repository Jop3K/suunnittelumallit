/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

/**
 *
 * @author Joni
 */

import java.util.Observable;

public class ClockTimer extends Observable implements Runnable {
    
    //muuttujat
   public int hour;
   public int minute;
   public int second;

   public ClockTimer() {
        hour = 0;
        minute = 0;
        second = 0;
    }

   
    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setSecond(int second) {
        this.second = second;
    }
   
           
    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }
    
    // Lisätään sekunteja kun kello käy ja tarkistetaan milloin on aika lisätä minuutti ja tunti
       void tick() {
        second++;
        if (second >= 60) {
            second = 0;
            minute++;
            if (minute >= 60) {
                minute = 0;
                hour++;
                if (hour >= 24) {
                    hour = 0;
                }
            }
        }
        setChanged();
        notifyObservers();
    }

       //Asetetaan Thread suorittamaan sekunnin välein Thread.sleep(1000);
           @Override
    public void run() {
        while (true) {
            tick();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                
            }
        }
    }

}
