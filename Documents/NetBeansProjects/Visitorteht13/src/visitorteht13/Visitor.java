package visitorteht13;

public interface Visitor {
	void visit(Charmander state);
	void visit(Charmeleon state);
	void visit(Charizard state);
}
