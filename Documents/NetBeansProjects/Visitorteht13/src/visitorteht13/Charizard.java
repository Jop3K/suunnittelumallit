package visitorteht13;

public class Charizard implements State {
	
	private int bonusPoints;
	public int getBonusPoints() {
		return bonusPoints;
	}
	public void setBonusPoints(int bonusPoints) {
		this.bonusPoints = bonusPoints;
	}
	
	private Charizard() {}
	private static final Charizard instance = new Charizard();
	public static State getInstance() {
		return instance;
	}
	
	public void evolve(final Pokemon pokemon) {}

	public void printStage() {
		System.out.println("Charizard saa "+bonusPoints+" bonus pistettä!");
	}
	
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}	
}
