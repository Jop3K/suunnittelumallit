package visitorteht13;

public class Charmander implements State {
	
	private int bonusPoints;
	public int getBonusPoints() {
		return bonusPoints;
	}
	public void setBonusPoints(int bonusPoints) {
		this.bonusPoints = bonusPoints;
	}

	private Charmander() {}
	private static final Charmander instance = new Charmander();
	public static State getInstance() {
		return instance;
	}
	
	public void evolve(final Pokemon pokemon) {
		pokemon.setState(Charmeleon.getInstance());
	}
	
	public void printStage() {
		System.out.println("Charmander saa "+bonusPoints+" bonus pistettä!");
	}
	
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
