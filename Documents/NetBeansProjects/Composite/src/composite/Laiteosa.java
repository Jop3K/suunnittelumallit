package composite;
/*
* @author Joni
*
*/
public interface Laiteosa {

    public double getHinta();
    public void lisaaLaiteosa(Laiteosa osa);

}
