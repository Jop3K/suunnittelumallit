package composite;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {
    
/*
* @author Joni
*
*/
    public static void main(String[] args) {
        
   
      
        
        LaiteosaTehdas tehdas = loadTehdas("ChinaTehdas");
        
        Laiteosa tietokone = tehdas.valmistaKotelo();
        
        Laiteosa emolevy = tehdas.valmistaEmolevy();
        
        tietokone.lisaaLaiteosa(emolevy);
        
        Laiteosa prosessori = tehdas.valmistaProsessori();
        Laiteosa muisti = tehdas.valmistaMuistipiiri();
        Laiteosa naytonohjain = tehdas.valmistaNaytonohjain();
        Laiteosa verkkokortti = tehdas.valmistaVerkkokortti();
        
        emolevy.lisaaLaiteosa(prosessori);
        emolevy.lisaaLaiteosa(muisti);
        emolevy.lisaaLaiteosa(naytonohjain);
        emolevy.lisaaLaiteosa(verkkokortti);
        
        System.out.println("Tietokoneen kokoonpanon yhteishinta on: " + tietokone.getHinta() + " euroa");
        
    }
    
        public static LaiteosaTehdas loadTehdas(String mikaTehdas) {
        LaiteosaTehdas tehdas = null;
        Class c = null;

        Properties properties = new Properties();
        try {
            properties.load(
                    new FileInputStream("C:\\Users\\Joni\\Documents\\NetBeansProjects\\Composite\\src\\composite\\tehdas.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
//Luetaan tehdas properties tiedostosta
            c = Class.forName(properties.getProperty(mikaTehdas));
            tehdas = (LaiteosaTehdas) c.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return tehdas;
    }

}
