
package composite;

/*
* @author Joni
*
*/
public interface LaiteosaTehdas {
    
    public Laiteosa valmistaKotelo();
    public Laiteosa valmistaEmolevy();
    public Laiteosa valmistaProsessori();
    public Laiteosa valmistaMuistipiiri();
    public Laiteosa valmistaNaytonohjain();
    public Laiteosa valmistaVerkkokortti();

}
