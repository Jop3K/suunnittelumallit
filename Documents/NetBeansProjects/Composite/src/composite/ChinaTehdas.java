package composite;
/*
* @author Joni
*
*/

public class ChinaTehdas implements LaiteosaTehdas {

    @Override
    public Laiteosa valmistaKotelo() {
        return new ChinaKotelo();
    }

    @Override
    public Laiteosa valmistaEmolevy() {
        return new ChinaEmolevy();
    }

    @Override
    public Laiteosa valmistaProsessori() {
        return new ChinaProsessori();
    }

    @Override
    public Laiteosa valmistaMuistipiiri() {
        return new ChinaMuistipiiri();
    }

    @Override
    public Laiteosa valmistaNaytonohjain() {
        return new ChinaNaytonohjain();
    }

    @Override
    public Laiteosa valmistaVerkkokortti() {
        return new ChinaVerkkokortti();
    }

}
