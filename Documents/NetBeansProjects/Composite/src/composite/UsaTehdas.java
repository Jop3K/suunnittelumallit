package composite;

/*
* @author Joni
*
*/
public class UsaTehdas implements LaiteosaTehdas {

    @Override
    public Laiteosa valmistaKotelo() {
        return new UsaKotelo();
    }

    @Override
    public Laiteosa valmistaEmolevy() {
        return new UsaEmolevy();
    }

    @Override
    public Laiteosa valmistaProsessori() {
        return new UsaProsessori();
    }

    @Override
    public Laiteosa valmistaMuistipiiri() {
        return new UsaMuistipiiri();
    }

    @Override
    public Laiteosa valmistaNaytonohjain() {
        return new UsaNaytonohjain();
    }

    @Override
    public Laiteosa valmistaVerkkokortti() {
        return new UsaVerkkokortti();
    }

}
