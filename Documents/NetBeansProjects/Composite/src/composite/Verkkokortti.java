package composite;
/*
* @author Joni
*
*/
abstract class Verkkokortti implements Laiteosa {

    private double hinta;

    public Verkkokortti(double hinta) {
        this.hinta = hinta;
    }

    @Override
    public double getHinta() {
        return hinta;
    }

    @Override
    public void lisaaLaiteosa(Laiteosa osa) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
