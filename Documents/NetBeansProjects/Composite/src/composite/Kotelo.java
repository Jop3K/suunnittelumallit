package composite;

import java.util.ArrayList;
import java.util.List;

/*
* @author Joni
*
*/
abstract class Kotelo implements Laiteosa  {
    
    private double hinta;
    private List<Laiteosa> osat;

    public Kotelo(double hinta) {
        this.hinta = hinta;
        osat = new ArrayList<>();
    }

    @Override
    public double getHinta() {
        for (Laiteosa osa : osat) {
            hinta += osa.getHinta();
        }
        return hinta;
    }

    @Override
    public void lisaaLaiteosa(Laiteosa osa) {
        osat.add(osa);
    }

}
