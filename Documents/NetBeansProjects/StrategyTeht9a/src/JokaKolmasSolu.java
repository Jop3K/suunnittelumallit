
import java.util.List;
import java.util.ListIterator;

// Iteraattori
public class JokaKolmasSolu implements ListConverter {
	private String result;
	private int count;
	ListIterator<String> itr;
	
	public String listToString(List<String> list) {
		result = null;
		count = 1;
		
		itr = list.listIterator();
		
		while(itr.hasNext()) {
			if(count == 1) {
				result = itr.next() + " ";
			}
			else if(count % 3 == 0) {
				result = result + itr.next() + "\n";
			}
			else {
				result = result + itr.next() + " ";
			}
			count++;
		}
		
		return result;
	}

}
