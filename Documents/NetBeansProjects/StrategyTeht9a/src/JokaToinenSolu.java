
import java.util.List;


// lista taulukoksi
public class JokaToinenSolu implements ListConverter {
	private String result;
	public String listToString(List<String> list) {
		result = null;
		String[] resultArray = new String[list.size()];
		resultArray = list.toArray(resultArray);
		
		for (int i = 1; i < resultArray.length + 1; i++) {
			if( i == 1) {
				result = resultArray[i-1] + " ";
			}
			else if(i % 2 == 0) {
				result = result + resultArray[i-1] + "\n";
			}
			else {
				result = result + resultArray[i-1] + " ";
			}
		}
		return result;
	}
}