

import java.util.ArrayList;
import java.util.List;

/*
* @author Joni
*/

public class Main {

    
	public static void main(String[] args) {
		ListConverter js = new JokainenSolu();
		ListConverter jts = new JokaToinenSolu();
		ListConverter jks = new JokaKolmasSolu();
		ListPrinter printer = new ListPrinter(js);
		List<String> cells = new ArrayList<String>();
                
                
		cells.add("Roses");
		cells.add("are");
		cells.add("red");
		cells.add("Violets");
		cells.add("are");
		cells.add("blue");
		cells.add("Sugar");
		cells.add("is");
                cells.add("sweet");
                cells.add("and");
                cells.add("so");
                cells.add("are");
                cells.add("you");
                
		printer.print(cells);
		printer.setConverter(jts);
		printer.print(cells);
		printer.setConverter(jks);
		printer.print(cells);
	}
}
