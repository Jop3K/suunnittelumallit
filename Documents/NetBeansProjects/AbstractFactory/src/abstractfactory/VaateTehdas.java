package abstractfactory;

/*
* @author Joni
*
*/
public interface VaateTehdas {
    
    public abstract Vaate valmistaFarkut();
    public abstract Vaate valmistaPaita();
    public abstract Vaate valmistaLippis();
    public abstract Vaate valmistaKengat();
    
}
