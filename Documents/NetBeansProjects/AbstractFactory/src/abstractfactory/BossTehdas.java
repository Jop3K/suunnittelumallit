package abstractfactory;

/*
* @author Joni
*
*/
public class BossTehdas implements VaateTehdas {

    @Override
    public Vaate valmistaFarkut() {
        return new BossFarkut();
    }

    @Override
    public Vaate valmistaPaita() {
        return new BossPaita();
    }

    @Override
    public Vaate valmistaLippis() {
        return new BossLippis();
    }

    @Override
    public Vaate valmistaKengat() {
        return new BossKengat();
    }


}
