package abstractfactory;

/*
* @author Joni
*
*/

public class AdidasTehdas implements VaateTehdas {

    @Override
    public Vaate valmistaFarkut() {
        return new AdidasFarmari();
    }

    @Override
    public Vaate valmistaPaita() {
        return new AdidasPaita();
    }

    @Override
    public Vaate valmistaLippis() {
        return new AdidasLippis();
    }

    @Override
    public Vaate valmistaKengat() {
        return new AdidasKengat();
    }


}
