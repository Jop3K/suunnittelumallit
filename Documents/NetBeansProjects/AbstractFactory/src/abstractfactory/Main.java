package abstractfactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/*
* @author Joni
*
*/
public class Main {

    public static void main(String[] args) {

        Koodaaja jasperKoodaja = new JasperKoodaaja("Jasper");

        jasperKoodaja.pueVaatteet(loadTehdas("AdidasTehdas"));

        System.out.println(jasperKoodaja);
        System.out.println("");

        jasperKoodaja = new JasperKoodaaja("Jasper");

        jasperKoodaja.pueVaatteet(loadTehdas("BossTehdas"));

        System.out.println(jasperKoodaja);
        System.out.println("");

    }
    
    // Ladataan tehdas tiedostosta
    public static VaateTehdas loadTehdas(String mikaTehdas) {
        VaateTehdas tehdas = null;
        Class c = null;

        Properties properties = new Properties();
        try {
            properties.load(
                    new FileInputStream("C:\\Users\\Joni\\Documents\\NetBeansProjects\\AbstractFactory\\src\\abstractfactory\\tehdas.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {

            c = Class.forName(properties.getProperty(mikaTehdas));
            tehdas = (VaateTehdas) c.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return tehdas;
    }

}
