package abstractfactory;

import java.util.ArrayList;
import java.util.List;

/*
* @author Joni
*
*/

public class JasperKoodaaja implements Koodaaja {

    private String nimi;
    private List<Vaate> vaatteet;

    public JasperKoodaaja(String nimi) {
        this.nimi = nimi;
        vaatteet = new ArrayList<>();
    }

    public void pueVaatteet(VaateTehdas tehdas) {
        vaatteet.add(tehdas.valmistaFarkut());
        vaatteet.add(tehdas.valmistaPaita());
        vaatteet.add(tehdas.valmistaLippis());
        vaatteet.add(tehdas.valmistaKengat());
    }

    @Override
    public String toString() {
        String tiedot = "Hei! nimeni on " + nimi;
        String vaatteet = ", minulla on päällä: ";
        if (!(this.vaatteet.isEmpty())) {
            for (Vaate v : this.vaatteet) {
                vaatteet += v.toString() + ", ";
            }
            return tiedot + vaatteet.substring(0, vaatteet.length() - 2);
        }
        
        return tiedot;
    }

}
