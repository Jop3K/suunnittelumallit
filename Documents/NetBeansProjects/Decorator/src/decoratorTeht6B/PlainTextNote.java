package decoratorTeht6B;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PlainTextNote implements Muistio {

    private final File file;
    private Scanner reader;
    private FileWriter fw;
    private String note;

    public PlainTextNote(File file) {
        super();
        this.file = file;
    }

    @Override
    public String lue() {

        try {
            reader = new Scanner(file);
            note = reader.nextLine();
            return note;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PlainTextNote.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return note;
    }

    @Override
    public void kirjoita(String note) {

        try {
            fw = new FileWriter(this.file);
            fw.write(note);
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(PlainTextNote.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public String toString() {
        return note;
    }

}
