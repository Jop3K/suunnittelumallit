package decoratorTeht6B;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class MuistioUI {

    private final Scanner reader;

    public MuistioUI() {
        reader = new Scanner(System.in);
    }

    public void start() throws IOException {
        System.out.println("Tervetuloa muistiinpano ohjelmaan!\n");

        OUTER:
        while (true) {
            menu();
            String selection = reader.nextLine();
            System.out.println("");
            switch (selection) {
                case "4":
                    break OUTER;
                case "1":
                    uusiMuistio();
                    break;
                case "2":
                    lueMuistio();
                    break;
                case "3":
                    lueSalakirjoitettuMuistio();
                    break;
                default:
                    System.out.println("Yritä uudestaan\n");
                    break;
            }
        }

    }

    public void menu() {
        System.out.println("Kirjoittaaksesi uuden muistion, Syötä: 1");
        System.out.println("Lukeaksesi muistion, Syötä: 2");
        System.out.println("Lukeaksesi salakirjoitetun muistion, Syötä: 3");
        System.out.println("Haluatko poistua?, Syötä: 4");
        System.out.println("");
        System.out.print("Valintasi: ");
    }

    public void uusiMuistio() throws IOException {
        System.out.print("Syötä tiedoston nimi: ");
        String filename = reader.nextLine();
        System.out.println("");
        File file = new File(filename);
        if (file.exists()) {
            System.out.println("Tiedosto on jo olemassa!");
            while (true) {
                System.out.print("Haluatko ylikirjoittaa tiedoston? (y/n): ");
                String selection = reader.nextLine();
                if (selection.equalsIgnoreCase("n")) {
                    uusiMuistio();
                } else {
                    break;
                }
            }
        }

        System.out.print("Muistio: ");
        String note = reader.nextLine();

        Muistio uusiMuistio = new PlainTextNote(file);
        uusiMuistio.kirjoita(note);

        while (true) {
            System.out.print("Haluatko salakirjoittaa muistion? (y/n): ");
            String selection = reader.nextLine();
            System.out.println("");
            if (selection.equalsIgnoreCase("y")) {
                salakirjoitaMuistio(uusiMuistio, note);
                break;
            } else {
                break;
            }
        }
    }

    public void salakirjoitaMuistio(Muistio newNote, String note) {
        CaesarCipherDecorator encryptedNote = new CaesarCipherDecorator(newNote);
        encryptedNote.kirjoita(note);
    }

    public void lueMuistio() {
        System.out.print("Syötä tiedoston nimi: ");
        String filename = reader.nextLine();
        File file = new File(filename);
        Muistio readableNote = new PlainTextNote(file);
        System.out.println("\nMuistiinpanosi: " + readableNote.lue());
        System.out.println("");
    }

    public void lueSalakirjoitettuMuistio() {
        System.out.print("Syötä tiedoston nimi: ");
        String filename = reader.nextLine();
        File file = new File(filename);
        NoteDecorator decryptedNote = new CaesarCipherDecorator(new PlainTextNote(file));
        System.out.println("\nMuistiinpanosi: " + decryptedNote.lue());
        System.out.println("");
    }
}
