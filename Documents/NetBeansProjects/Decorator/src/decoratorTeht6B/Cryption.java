package decoratorTeht6B;


public interface Cryption {
    
    String encrypt(String note);
    String decrypt(String note);
    
}
