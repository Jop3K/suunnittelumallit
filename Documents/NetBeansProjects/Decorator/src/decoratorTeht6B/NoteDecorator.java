package decoratorTeht6B;

public abstract class NoteDecorator implements Muistio {

    protected Muistio noteToBeDecorated;

    public NoteDecorator(Muistio noteToBeDecorated) {
        this.noteToBeDecorated = noteToBeDecorated;
    }

    @Override
    public String lue() {
        return noteToBeDecorated.lue();
    }
    
    @Override
    public void kirjoita(String note) {
        noteToBeDecorated.kirjoita(note);
    }

}
