package decoratorTeht6B;

public class CaesarCipherDecorator extends NoteDecorator implements Cryption {

    private final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public CaesarCipherDecorator(Muistio noteToBeDecorated) {
        super(noteToBeDecorated);
    }

    @Override
    public String lue() {
        return decrypt(super.lue());
    }

    @Override
    public void kirjoita(String note) {
        super.kirjoita(encrypt(note));
    }

    @Override
    public String decrypt(String note) {

        String cipherText = noteToBeDecorated.lue().toUpperCase();
        String plainText = "";
        for (int i = 0; i < cipherText.length(); i++) {
            if (!contains(cipherText.charAt(i)))  {
                plainText += cipherText.charAt(i);
                continue;
            }
            int charPosition = ALPHABET.indexOf(cipherText.charAt(i));
            int keyVal = (charPosition - 3) % 26;
            if (keyVal < 0) {
                keyVal = ALPHABET.length() + keyVal;
            }
            char replaceVal = ALPHABET.charAt(keyVal);
            plainText += replaceVal;
        }

        return plainText;

    }

    @Override
    public String encrypt(String note) {
        String plainText = noteToBeDecorated.lue().toUpperCase();
        String cipherText = "";
        for (int i = 0; i < plainText.length(); i++) {
            if (!contains(plainText.charAt(i)))  {
                cipherText += plainText.charAt(i);
                continue;
            }
            int charPosition = ALPHABET.indexOf(plainText.charAt(i));
            int keyVal = (3 + charPosition) % 26;
            char replaceVal = ALPHABET.charAt(keyVal);
            cipherText += replaceVal;
        }

        return cipherText;
    }

    public boolean contains(char chr) {
        return ALPHABET.indexOf(chr) != -1;
    }

}
